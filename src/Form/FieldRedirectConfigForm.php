<?php

namespace Drupal\field_redirect\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_redirect\Utils\Utility;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementing system configuration forms.
 */
class FieldRedirectConfigForm extends ConfigFormBase {

  /**
   * Variable of cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend) {
    $this->setConfigFactory($config_factory);
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return Utility::MODULE_NAME . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config(Utility::MODULE_NAME . '.settings');

    // Source text field.
    $form['mappings'] = [
      '#type' => 'textarea',
      '#rows' => 10,
      '#title' => $this->t('The redirect mapping settings of entity (type and bundle) and fields:'),
      '#default_value' => Utility::toString($config->get(Utility::MODULE_NAME . '.mappings')),
      '#description' => $this->t('Enter one value per line, in the format entity_type:entity_bundle|field_machine_name(s).<br>* When specifying multiple field_machine_names, separate them with commas (,) and describe them in order of priority.<br>* Can also specify one status (#403 or #404) at the end of field_machine_name.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $element = $form['mappings'] ?? [];
    $map_string = $form_state->getValue('mappings');
    $map_array = Utility::toArray($map_string);
    if (!\strlen(\trim($map_string))) {
      $form_state->setValueForElement($element, NULL);
      return;
    }
    elseif (!Utility::isValidMappings($form_state, $element, $map_string)) {
      return;
    }
    $form_state->setValueForElement($element, $map_array);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(Utility::MODULE_NAME . '.settings');
    $config->set(Utility::MODULE_NAME . '.mappings', $form_state->getValue('mappings'));
    $config->save();
    $this->cacheBackend->invalidateAll();
    \drupal_flush_all_caches();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      Utility::MODULE_NAME . '.settings',
    ];
  }

}
