<?php

namespace Drupal\field_redirect\Utils;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Utility for Field Redirect Module.
 */
class Utility {

  const MODULE_NAME = 'field_redirect';
  const ENTITY_PAIR_SEPARATOR = ':';
  const ENTITY_BUNDLE_WILDCARD = '*';
  const PAIR_SEPARATOR = '|';
  const FIELDS_SEPARATOR = ',';

  /**
   * Implements toString().
   *
   * @param null|array $array
   *   The mapping array.
   *
   * @return string
   *   The return value.
   */
  public static function toString($array = []) {
    $return = [];
    foreach ((array) $array as $entity_type_bundle => $field_machine_names_array) {
      $field_machine_names = \implode(self::FIELDS_SEPARATOR, (array) $field_machine_names_array);
      $return[] = $entity_type_bundle . self::PAIR_SEPARATOR . $field_machine_names;
    }
    return \implode(PHP_EOL, $return);
  }

  /**
   * Implements toArray().
   *
   * @param string $mappings
   *   The mapping string.
   *
   * @return array
   *   The return value.
   */
  public static function toArray($mappings = '') {
    $return = [];
    if (\strlen($mappings)) {
      if ($lines = \explode(\PHP_EOL, $mappings)) {
        foreach ($lines as $pair) {
          if (\strlen($pairs = \trim($pair))) {
            $entity_fields = \array_map('trim', \explode(self::PAIR_SEPARATOR, $pairs, 2));
            [$entity_type_bundle, $fields_str] = $entity_fields;
            $entity_type_bundle_array = \array_map('trim', \explode(self::ENTITY_PAIR_SEPARATOR, $entity_type_bundle));
            $fields_array = \array_map('trim', \explode(self::FIELDS_SEPARATOR, $fields_str));
            if (\count($entity_type_bundle_array) === 2 && !empty($fields_array)) {
              $entity_type_bundle = \implode(self::ENTITY_PAIR_SEPARATOR, $entity_type_bundle_array);
              $return[$entity_type_bundle] = $fields_array;
            }
          }
        }
      }
    }
    return $return;
  }

  /**
   * Implements isValidMappings().
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $element
   *   The form element.
   * @param string $mappings
   *   The mapping string.
   *
   * @return bool
   *   The return value.
   */
  public static function isValidMappings(FormStateInterface $form_state, array $element, $mappings = '') {
    $return = TRUE;
    if (\strlen($mappings)) {
      if ($lines = \explode(\PHP_EOL, $mappings)) {
        $line_number = 0;
        $error_t_args = [];
        $duplicates_entity = [];
        foreach ($lines as $pair) {
          if (\strlen($pairs = \trim($pair))) {
            ++$line_number;
            $pairs = \array_map('trim', \explode(self::PAIR_SEPARATOR, $pairs, 2));
            $entity_type_bundle_pair = \array_shift($pairs) ?? '';
            $field_machine_name_str = \array_shift($pairs) ?? '';
            $entity_type_bundle = \array_map('trim', \explode(self::ENTITY_PAIR_SEPARATOR, $entity_type_bundle_pair, 2));
            $entity_type = \array_shift($entity_type_bundle) ?? '';
            $entity_bundle = \array_shift($entity_type_bundle) ?? '';
            if (\strlen($entity_type) > 0 && \strlen($entity_bundle) > 0 && \strlen($field_machine_name_str) > 0) {
              if (empty($duplicates_entity[$entity_type_bundle_pair])) {
                $duplicates_entity[$entity_type_bundle_pair] = 0;
              }
              $duplicates_entity[$entity_type_bundle_pair]++;
              $entity_config_name = '';
              $entity_config_target = '';
              switch ($entity_type) {
                case 'node':
                  $entity_config_name = 'node.type.' . $entity_bundle;
                  $entity_config_target = 'type';
                  break;

                case 'taxonomy_term':
                  $entity_config_name = 'taxonomy.vocabulary.' . $entity_bundle;
                  $entity_config_target = 'vid';
                  break;

                case 'user':
                  $entity_config_name = 'user.settings';
                  $entity_config_target = 'anonymous';
                  break;

                default:
                  $error_t_args[] = \t('Line @line_number: Entity type "@entity_type" is not supported.', [
                    '@line_number' => $line_number,
                    '@entity_type' => $entity_type,
                  ])->__toString();
                  continue 2;
              }
              $field_config_name_prefix = '';
              $target_config_value = \Drupal::config($entity_config_name)->get($entity_config_target);
              if ($entity_bundle === self::ENTITY_BUNDLE_WILDCARD) {
                $field_config_name_prefix = 'field.field.' . $entity_type . '.';
              }
              elseif (FALSE
                || ($entity_type === 'user' && \strlen($target_config_value))
                || $target_config_value === $entity_bundle
              ) {
                $field_config_name_prefix = 'field.field.' . $entity_type . '.' . $entity_bundle . '.';
              }
              else {
                $error_t_args[] = \t('Line @line_number: The bundle name "@entity_bundle" does not exist for entity type "@entity_type".', [
                  '@line_number' => $line_number,
                  '@entity_type' => $entity_type,
                  '@entity_bundle' => $entity_bundle,
                ])->__toString();
                continue;
              }
              $field_machine_names = \array_map('trim', \explode(self::FIELDS_SEPARATOR, $field_machine_name_str));
              $duplicates_field = [];
              $max_i = \count($field_machine_names) - 1;
              foreach ($field_machine_names as $current_i => $field_machine_name) {
                if (empty($duplicates_field[$field_machine_name])) {
                  $duplicates_field[$field_machine_name] = 0;
                }
                $duplicates_field[$field_machine_name]++;
                if (\strpos($field_machine_name, '#') === 0) {
                  if ($field_machine_name !== '#403' && $field_machine_name !== '#404') {
                    $error_t_args[] = \t('Line @line_number: The specified status is invalid. Please specify "#403" or "#404".', [
                      '@line_number' => $line_number,
                    ])->__toString();
                  }
                  if ($max_i !== $current_i) {
                    $error_t_args[] = \t('Line @line_number: Status is can be specified only one at the end.', [
                      '@line_number' => $line_number,
                    ])->__toString();
                  }
                }
                else {
                  $field_config = NULL;
                  $config_factory = \Drupal::configFactory();
                  if ($entity_bundle === self::ENTITY_BUNDLE_WILDCARD) {
                    $match_fields = $config_factory->listAll($field_config_name_prefix);
                    foreach ((array) $match_fields as $field_config_name) {
                      $config = $config_factory->get($field_config_name);
                      if (!$config->isNew() && $config->get('field_name') === $field_machine_name) {
                        $field_config = $config;
                      }
                    }
                  }
                  else {
                    $config = $config_factory->get($field_config_name_prefix . $field_machine_name);
                    if (!$config->isNew() && $config->get('field_name') === $field_machine_name) {
                      $field_config = $config;
                    }
                  }
                  if ($field_config instanceof ImmutableConfig) {
                    if (!\in_array($field_config->get('field_type'), [
                      'link',
                      'file',
                      'image',
                    ], TRUE)) {
                      $error_t_args[] = \t('Line @line_number: The field type for "@field_machine_name" must be link, file or image.', [
                        '@line_number' => $line_number,
                        '@field_machine_name' => $field_machine_name,
                      ])->__toString();
                    }
                  }
                  else {
                    $error_t_args[] = \t('Line @line_number: The field for "@field_machine_name" does not exist.', [
                      '@line_number' => $line_number,
                      '@field_machine_name' => $field_machine_name,
                    ])->__toString();
                  }
                }
              }
              foreach ($duplicates_field as $field => $number) {
                if ($number > 1) {
                  if (\strpos($field, '#') === 0) {
                    $error_t_args[] = \t('Line @line_number: Can specify only one status.', [
                      '@line_number' => $line_number,
                    ])->__toString();
                  }
                  else {
                    $error_t_args[] = \t('Line @line_number: Fields for "@field_machine_name" are specified is duplicate.', [
                      '@line_number' => $line_number,
                      '@field_machine_name' => $field_machine_name,
                    ])->__toString();
                  }
                }
              }
            }
            else {
              $error_t_args[] = \t('Line @line_number: The mapping format is incorrect.', [
                '@line_number' => $line_number,
              ])->__toString();
            }
          }
        }
      }
    }
    foreach ($duplicates_entity as $entity_type_bundle => $number) {
      if ($number > 1) {
        $error_t_args[] = \t('Set of the entity type and bundle is duplicated. (@entity_type_bundle)', [
          '@entity_type_bundle' => $entity_type_bundle,
        ])->__toString();
      }
    }
    if (!empty($error_t_args)) {
      $form_state->setError($element, Markup::create(\implode('<br>', $error_t_args)));
      $return = FALSE;
    }
    return $return;
  }

  /**
   * Implements getRedirectFields().
   *
   * @param string $target_bundle
   *   The node bundle string.
   *
   * @return array
   *   The return redirect fields.
   */
  public static function getRedirectFields($target_bundle) {
    $config = \Drupal::config(Utility::MODULE_NAME . '.settings');
    $redirect_fields = $config->get(Utility::MODULE_NAME . '.mappings');
    return $redirect_fields[$target_bundle] ?? [];
  }

  /**
   * Implements getEntityTypeLabel().
   *
   * @param \Drupal\Core\Entity\EntityBase $entity
   *   The node bundle string.
   *
   * @return string
   *   The return entity type label.
   */
  public static function getEntityTypeLabel(EntityBase $entity) {
    $target_entity_type = $entity->getEntityTypeId();
    $target_entity_bundle = $entity->bundle();

    // Get entity type config.
    $target_config_name = '';
    $target_config_key = '';
    switch ($target_entity_type) {
      case 'node':
        $target_config_name = 'node.type.' . $target_entity_bundle;
        $target_config_key = 'name';
        break;

      case 'taxonomy_term':
        $target_config_name = 'taxonomy.vocabulary.' . $target_entity_bundle;
        $target_config_key = 'name';
        break;

      case 'user':
        return \t('User')->__toString();
    }

    // Return entity type label.
    return \Drupal::config($target_config_name)->get($target_config_key) ?? '[unknown]';
  }

}
